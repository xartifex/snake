package org.artifex.game;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * author: artifex
 * since: 9/23/14 12:01 AM
 */
public class Player
{
    private double x;
    private double y;

    private double velX;
    private double velY;

    private BufferedImage player;

    private final Game game;

    public Player(double x, double y, Game game)
    {
        this.x = x;
        this.y = y;
        this.game = game;

        SpriteSheet ss = new SpriteSheet(game.getSpriteSheet());

        player = ss.grabImage(1,1,32,32);
    }

    public void tick()
    {
        x+=velX;
        y+=velY;

        if(x > game.getWidth())
        {
            x = 0;
        }
        else if (x < -16)
        {
            x = game.getWidth();
        }

        if(y > game.getHeight())
        {
            y = 0;
        }
        else if (y < -16)
        {
            y = game.getHeight();
        }
    }

    public void render(Graphics g)
    {
        g.drawImage(player, (int)x, (int)y, null);
    }

    public double getX()
    {
        return x;
    }

    public void setX(double x)
    {
        this.x = x;
    }

    public double getY()
    {
        return y;
    }

    public void setY(double y)
    {

        this.y = y;
    }

    public void setVelX(double velX)
    {
        this.velX = velX;
    }

    public void setVelY(double velY)
    {
        this.velY = velY;
    }
}
