package org.artifex.game;

import java.awt.*;
import java.util.Random;

/**
 * author: artifex
 * since: 9/25/14 10:58 PM
 */
public class SnakeController
{
    private Snake snake;
    private Point snakeHead;
    private Fruit fruit;
    private Game game;
    private Random randomFruitPos;

    public SnakeController(Snake snake, Fruit fruit, Game game)
    {
        this.snake = snake;
        snakeHead = snake.getHead();
        this.fruit = fruit;
        this.game = game;
        randomFruitPos = game.getRandomFruitPos();
    }

    private boolean bumpedIntoItself(Rectangle snakeHeadBound)
    {
        for(Point snakeElement: snake.getSnakeWithoutHead())
        {
            Rectangle curBound = new Rectangle((int)snakeElement.getX(), (int)snakeElement.getY(), 32, 32);
            if(curBound.intersects(snakeHeadBound))
            {
                return true;
            }
        }
        return false;
    }

    public void tick()
    {
        Rectangle snakeHeadBound = new Rectangle((int)snakeHead.getX(), (int)snakeHead.getY(), 32, 32);
        if(snake.isHeadMovedAtLeastOnce() && bumpedIntoItself(snakeHeadBound))
        {
            snake.setCurCommand(Command.DIE);
            return;
        }


        if(snakeHeadBound.intersects(fruit.getBounds()))
        {
            snake.grow();
            snake.setSpeed(snake.getSpeed() - 4);
            fruit.setX(randomFruitPos.nextInt(game.getWidth()));
            fruit.setY(randomFruitPos.nextInt(game.getHeight()));
        }
    }

    public void render(Graphics g)
    {
        if(snake.getCurCommand() == Command.DIE)
        {
            Color backUp = g.getColor();
            g.setColor(Color.RED);
            g.drawString("GAME OVER!!!", game.getWidth() / 2, game.getHeight() / 2);
            g.setColor(backUp);
        }
    }
}
