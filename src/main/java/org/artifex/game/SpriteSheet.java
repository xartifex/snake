package org.artifex.game;

import java.awt.image.BufferedImage;

/**
 * author: artifex
 * since: 9/22/14 11:40 PM
 */
public class SpriteSheet
{
    private BufferedImage bufferedImage;

    public SpriteSheet(BufferedImage bufferedImage)
    {
        this.bufferedImage = bufferedImage;
    }

    public BufferedImage grabImage(int col, int row, int width, int height)
    {
        return bufferedImage.getSubimage((col * 32) - 32, (row * 32) - 32, width, height);
    }
}
