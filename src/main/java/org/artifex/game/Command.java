package org.artifex.game;

/**
 * author: artifex
 * since: 9/25/14 10:02 PM
 */
public enum Command
{
    NONE, LEFT, RIGHT, UP, DOWN, DIE;
}
