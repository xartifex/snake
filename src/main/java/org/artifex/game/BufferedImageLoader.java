package org.artifex.game;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * author: artifex
 * since: 9/22/14 11:36 PM
 */
public class BufferedImageLoader
{
    private BufferedImage image;

    public BufferedImage loadImage(String path) throws IOException
    {
        image = ImageIO.read(getClass().getResource(path));
        return image;
    }

}

