package org.artifex.game;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * author: artifex
 * since: 9/23/14 12:28 AM
 */
public class KeyInput extends KeyAdapter
{
    private final Game game;

    public KeyInput(Game game)
    {
        this.game = game;
    }

    @Override
    public void keyPressed(KeyEvent e)
    {
        game.keyPressed(e);
    }

    @Override
    public void keyReleased(KeyEvent e)
    {
        game.keyReleased(e);
    }
}
