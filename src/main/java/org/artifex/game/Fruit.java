package org.artifex.game;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * author: artifex
 * since: 9/25/14 10:47 PM
 */
public class Fruit
{
    private double x;
    private double y;

    private BufferedImage fruit;

    private final Game game;

    public Fruit(double x, double y, Game game)
    {
        this.x = x;
        this.y = y;
        this.game = game;

        SpriteSheet ss = new SpriteSheet(game.getSpriteSheet());

        fruit = ss.grabImage(2,1,32,32);
    }

    public void tick()
    {
    }

    public void render(Graphics g)
    {
        g.drawImage(fruit, (int)x, (int)y, null);
    }

    public double getX()
    {
        return x;
    }

    public void setX(double x)
    {
        this.x = x;
    }

    public double getY()
    {
        return y;
    }

    public void setY(double y)
    {

        this.y = y;
    }

    public Rectangle getBounds()
    {
        return new Rectangle((int)x, (int)y, 32, 32);
    }
}
