package org.artifex.game;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

/**
 * author: artifex
 * since: 9/25/14 9:45 PM
 */
public class Snake
{
    private boolean isDead;

    private static final int STEP = 32;

    private int speed = 50;
    private int counter;

    private int size;

    private List<Point> snakeWithoutHead;
    private BufferedImage snakeElementImg;
    private BufferedImage snakeHeadImg;
    private Point head;


    private Command curCommand;

    private final Game game;


    public Snake(double x, double y, int size , Game game)
    {
        this.size = size;
        this.game = game;
        curCommand = Command.NONE;

        SpriteSheet ss = new SpriteSheet(game.getSpriteSheet());

        snakeElementImg = ss.grabImage(1,1,32,32);
        snakeHeadImg = ss.grabImage(1,2,32,32);

        init(x, y);
    }

    private void init(double x, double y)
    {
        snakeWithoutHead = new LinkedList<Point>();
        head = new Point((int)x, (int)y);
        for(int i = 0; i < size - 1; i++)
        {
            snakeWithoutHead.add(new Point((int) x, (int) y));
        }

    }

    private boolean headMovedAtLeastOnce;

    public void tick()
    {
        if(isDead())
        {
            return;
        }

        counter++;
        if(counter > speed)
        {
            counter = 0;
        }
        else
        {
            return;
        }

        if(head.x > game.getWidth())
        {
            head.x = 0;
        }
        else if (head.x < -16)
        {
            head.x = game.getWidth();
        }

        if(head.y > game.getHeight())
        {
            head.y = 0;
        }
        else if (head.y < -16)
        {
            head.y = game.getHeight();
        }

        Point prevPos = head.getLocation();
        switch (curCommand)
        {
            case LEFT:
            {
                head.x+= -STEP;
                headMovedAtLeastOnce = true;
                break;
            }
            case RIGHT:
            {
                head.x+= STEP;
                headMovedAtLeastOnce = true;
                break;
            }
            case UP:
            {
                head.y+= -STEP;
                headMovedAtLeastOnce = true;
                break;
            }
            case DOWN:
            {
                head.y+= STEP;
                headMovedAtLeastOnce = true;
                break;
            }
            case DIE:
            {
                isDead = true;
            }
        }

        Point curLoc;
        for(Point snakeElement: snakeWithoutHead)
        {
            curLoc = snakeElement.getLocation();
            snakeElement.setLocation(prevPos);
            prevPos = curLoc;
        }
    }

    public boolean isHeadMovedAtLeastOnce()
    {
        return headMovedAtLeastOnce;
    }

    public void render(Graphics g)
    {
        for(Point snakeElement: snakeWithoutHead)
        {
            g.drawImage(snakeElementImg, (int)snakeElement.getX(), (int)snakeElement.getY(), null);
        }
        g.drawImage(snakeHeadImg, head.x, head.y, null);

        Color backUp = g.getColor();
        g.setColor(Color.WHITE);
        g.drawString("SIZE: " + (snakeWithoutHead.size() + 1), 10, 10);
        g.drawString("SPEED: " + (speed >  0 ? speed : "MAX"), 10, 25);
        g.setColor(backUp);

    }

    public void setCurCommand(Command curCommand)
    {
        this.curCommand = curCommand;
    }

    public int getSpeed()
    {
        return speed;
    }

    public void setSpeed(int speed)
    {
        this.speed = speed;
    }

    public Point getHead()
    {
        return head;
    }

    public void grow()
    {
        headMovedAtLeastOnce = false;
        snakeWithoutHead.add(new Point(head.x, head.y));
    }

    public List<Point> getSnakeWithoutHead()
    {
        return snakeWithoutHead;
    }

    public Command getCurCommand()
    {
        return curCommand;
    }

    public boolean isDead()
    {
        return isDead;
    }
}


