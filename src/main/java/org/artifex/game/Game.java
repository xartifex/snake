package org.artifex.game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

/**
 * author: artifex
 * since: 9/21/14 3:16 PM
 */
public class Game extends Canvas implements Runnable
{
    public static final int OPTIMAL_SLEEP_TIME_MILLIS = 10;
    public static final int WIDTH = 320;
    public static final int HEIGHT = WIDTH / 12 * 9;
    public static final int SCALE = 2;
    public static final String TITLE = "Smiley Snake";

    private boolean running;
    private Thread thread;

    private final BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
    private BufferedImage spriteSheet;

    private Snake snake;
    private Fruit fruit;
    private Random randomFruitPos;

    private SnakeController snakeController;

    private synchronized void start()
    {
        if (running)
        {
            return;
        }
        running = true;
        thread = new Thread(this);
        thread.start();
    }

    private synchronized void stop()
    {
        if(!running)
        {
            return;
        }
        try
        {
            thread.join();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        running = false;
    }

    private void init()
    {
        requestFocus();

        BufferedImageLoader loader = new BufferedImageLoader();
        try
        {
            spriteSheet = loader.loadImage("/gameFiddleSprite.png");
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        addKeyListener(new KeyInput(this));

        snake = new Snake(100, 100, 3, this);
        randomFruitPos = new Random();
        fruit = new Fruit(randomFruitPos.nextInt(getWidth()), randomFruitPos.nextInt(getHeight()), this);
        snakeController = new SnakeController(snake, fruit, this);
    }

    public Random getRandomFruitPos()
    {
        return randomFruitPos;
    }

    public void run()
    {
        init();
        long lastTime = System.nanoTime();
        final double amountOfTicks = 60.0;
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;
        int updates = 0;
        int frames = 0;
        long timer = System.currentTimeMillis();
        while (running)
        {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            if(delta >= 1)
            {
                tick();
                updates++;
                delta--;
            }
            render();
            frames++;

            if(System.currentTimeMillis() - timer > 1000)
            {
                timer = System.currentTimeMillis();
                System.out.println(updates + " Ticks, Fps " + frames);
                updates = 0;
                frames = 0;
            }
            try
            {
                Thread.sleep(OPTIMAL_SLEEP_TIME_MILLIS);
            }
            catch (InterruptedException e)
            {
                throw new RuntimeException(e);
            }
        }
        stop();
    }

    private void tick()
    {
        snake.tick();
        snakeController.tick();
    }

    private void render()
    {
        BufferStrategy bs = this.getBufferStrategy();
        //do we need to check on this condition all the time?
        if(bs == null)
        {
            createBufferStrategy(3);
            return;
        }

        Graphics g = bs.getDrawGraphics();

        g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
        snake.render(g);
        fruit.render(g);
        snakeController.render(g);

        g.dispose();
        bs.show();
    }

    public BufferedImage getSpriteSheet()
    {
        return spriteSheet;
    }

    public void keyPressed(KeyEvent e)
    {
        switch (e.getKeyCode())
        {
            case KeyEvent.VK_LEFT:
            {
                snake.setCurCommand(Command.LEFT);
                break;
            }
            case KeyEvent.VK_RIGHT:
            {
                snake.setCurCommand(Command.RIGHT);
                break;
            }
            case KeyEvent.VK_UP:
            {
                snake.setCurCommand(Command.UP);
                break;
            }
            case KeyEvent.VK_DOWN:
            {
                snake.setCurCommand(Command.DOWN);
                break;
            }
        }
    }

    public void keyReleased(KeyEvent e)
    {
    }

    public static void main(String[] args)
    {
        Game game = new Game();
        game.setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
        game.setMaximumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
        game.setMinimumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));

        JFrame frame = new JFrame(TITLE);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.add(game);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        game.start();
    }
}
